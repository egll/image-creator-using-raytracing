// Transform.cpp: implementation of the Transform class.

// Note: when you construct a matrix using mat4() or mat3(), it will be COLUMN-MAJOR
// Keep this in mind in readfile.cpp and display.cpp
// See FAQ for more details or if you're having problems.

#include "Transform.h"

// Helper rotation function.  Please implement this.  
mat3 Transform::rotate(const float degrees, const vec3& axis) 
{
	vec3 vecNormal = glm::normalize(axis);
	float convert_radians = degrees * pi / 180.0;

	float cos_angle = glm::cos(convert_radians);
	float sin_angle = glm::sin(convert_radians);

	glm::mat3x3 identity3x3 = mat3(1.0f);
	glm::mat3x3 mat_times_transpose = glm::mat3(vecNormal.x * vecNormal.x, vecNormal.x * vecNormal.y, vecNormal.x * vecNormal.z,
												vecNormal.x * vecNormal.y, vecNormal.y * vecNormal.y, vecNormal.y * vecNormal.z,
												vecNormal.x * vecNormal.z, vecNormal.y * vecNormal.z, vecNormal.z * vecNormal.z);
	glm::mat3x3 conjugate_transpose = glm::mat3(0, vecNormal.z, -vecNormal.y,
												-vecNormal.z, 0, vecNormal.x,
												vecNormal.y, -vecNormal.x, 0);

	return ((cos_angle * identity3x3) + ((1 - cos_angle) * mat_times_transpose) + (sin_angle * conjugate_transpose));
}

void Transform::left(float degrees, vec3& eye, vec3& up) 
{
	eye = eye * rotate(-degrees, glm::normalize(up));
}

void Transform::up(float degrees, vec3& eye, vec3& up) 
{
	vec3 rotation_axis = glm::normalize(glm::cross(eye, up));
	mat3 rotation = rotate(degrees, rotation_axis);
	eye = rotation * eye;
	up = rotation * up;
}

mat4 Transform::lookAt(const vec3 &eye, const vec3 &center, const vec3 &up) 
{
	vec3 w = glm::normalize(eye);
	vec3 u = glm::normalize(glm::cross(up, w));
	vec3 v = glm::cross(w, u);

	mat4 matrix_x4 = glm::mat4x4(u.x, v.x, w.x, 0,
								 u.y, v.y, w.y, 0,
								 u.z, v.z, w.z, 0,
								 0, 0, 0, 1);
	
	mat4 transformation_mat4 = glm::mat4x4(1, 0, 0, 0,
										   0, 1, 0, 0,
										   0, 0, 1, 0,
										   -eye.x, -eye.y, -eye.z, 1);
	return matrix_x4 * transformation_mat4;
}

mat4 Transform::perspective(float fovy, float aspect, float zNear, float zFar)
{
    mat4 ret;
	float theta = (fovy / 2) * (pi / 180.0);
	float d = 1 / glm::tan(theta);
	float A = -1 * ((zFar + zNear) / (zFar - zNear));
	float B = -1 * ((2 * zFar*zNear) / (zFar - zNear));
	mat4 matPerspective = glm::mat4x4(d / aspect, 0, 0, 0,
									  0, d, 0, 0,
									  0, 0, A, -1,
									  0, 0, B, 0); 
	return matPerspective;
}

mat4 Transform::scale(const float &sx, const float &sy, const float &sz) 
{
	
	mat4 matScale = glm::mat4x4(sx, 0, 0, 0,
								0, sy, 0, 0,
								0, 0, sz, 0,
								0, 0, 0, 1);
	return matScale;
	
}

mat4 Transform::translate(const float &tx, const float &ty, const float &tz) 
{
	mat4 matTranslate = glm::mat4x4(1, 0, 0, 0,
									0, 1, 0, 0,
									0, 0, 1, 0,
									tx, ty, tz, 1);
	return matTranslate;
}

// To normalize the up direction and construct a coordinate frame.  
// As discussed in the lecture.  May be relevant to create a properly 
// orthogonal and normalized up. 
// This function is provided as a helper, in case you want to use it. 
// Using this function (in readfile.cpp or display.cpp) is optional.  

vec3 Transform::upvector(const vec3 &up, const vec3 & zvec) 
{
    vec3 x = glm::cross(up,zvec); 
    vec3 y = glm::cross(zvec,x); 
    vec3 ret = glm::normalize(y); 
    return ret; 
}


Transform::Transform()
{

}

Transform::~Transform()
{

}
