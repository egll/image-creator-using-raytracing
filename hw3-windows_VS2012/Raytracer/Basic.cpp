#include "Basic.h"
#include "math.h"
#include "MathMethods.h"
#include <iostream>

Vector::Vector()
{
	x = 0;
	y = 0;
	z = 0;
}

Vector::Vector(float x, float y, float z)
{
	this->x = x;
	this->y = y;
	this->z = z;
}

Vector Vector::operator+(const Vector& v) const
{
	return Vector(this->x + v.x, this->y + v.y, this->z + v.z);
}

Vector Vector::operator-(const Vector& v) const
{
	return Vector(this->x - v.x, this->y - v.y, this->z - v.z);
}

Vector Vector::cross(const Vector& v) const
{
	return Vector(this->y * v.z - this->z * v.y, this->z * v.x - this->x * v.z, this->x * v.y - this->y * v.x);
}

Vector Vector::operator*(float scalar) const
{
	return Vector(this->x * scalar, this->y * scalar, this->z * scalar);
}

float Vector::operator*(const Vector& v) const
{
	return this->dot(v);
}

Vector Vector::operator/(float scalar) const
{
	return Vector(this->x / scalar, this->y / scalar, this->z / scalar);
}

Vector Vector::normalize() const
{
	if (this->x != 0 || this->y != 0 || this->z != 0)
	{
		float mag = this->magnitude();
		return Vector(this->x / mag, this->y / mag, this->z / mag);
	}
	return *this;
}

float Vector::magnitude() const
{
	return sqrt(this->dot(*this));
}

float Vector::dot(const Vector& v) const
{
	return this->x * v.x + this->y * v.y + this->z * v.z;
}

/* Normal */
Normal::Normal()
{
	x = 0;
	y = 0;
	z = 0;
}

Normal::Normal(float x, float y, float z)
{
	if (x != 0 || y != 0 || z != 0)
	{
		Vector vector(x, y, z);
		vector = vector.normalize();
		this->x = vector.x;
		this->y = vector.y;
		this->z = vector.z;
	} 
	else
	{
		this->x = 0;
		this->y = 0;
		this->z = 0;
	}
}

Normal::Normal(Vector& v)
{
	Vector vector = v.normalize();
	x = vector.x;
	y = vector.y;
	z = vector.z;
}

Normal Normal::operator+(Vector& v) const
{
	Vector sum = (v + (*this)).normalize();
	return Normal(sum);
}

Normal Normal::operator-(Vector& v) const
{
	Vector sum = (v - (*this)).normalize();
	return Normal(sum);
}

/* Point */

Point::Point()
{
	x = 0;
	y = 0;
	z = 0;
}
Point::Point(float x, float y, float z)
{
	this->x = x;
	this->y = y;
	this->z = z;
}

Point Point::operator+(const Vector& v) const
{
	return Point(this->x + v.x, this->y + v.y, this->z + v.z);
}

Vector Point::operator+(const Point& v) const
{
	return Vector(this->x + v.x, this->y + v.y, this->z + v.z);
}

Point Point::operator-(const Vector& v) const
{
	return Point(this->x - v.x, this->y - v.y, this->z - v.z);
}
Vector Point::operator-(const Point& v) const
{
	return Vector(this->x - v.x, this->y - v.y, this->z - v.z);
}

/* Ray */

Ray::Ray()
{
	t_min = 0;
	t_max = 0;
}

Ray::Ray(Point position, Vector direction, float t_min, float t_max)
{
	this->position = position;
	this->direction = direction;
	this->t_min = t_min;
	this->t_max = t_max;
}

Point Ray::pointAtTime(float t) const
{
	return  position + direction * t;
}

void Ray::print() const
{
	printf("Point: %f, %f, %f\n", position.x, position.y, position.z);
	printf("Direction: %f, %f, %f\n\n", direction.x, direction.y, direction.z);
}

/* Matrix */

Matrix::Matrix()
{
	Matrix(1.0);
}


Matrix::Matrix(float a)
{
	for (int i = 0; i < 4; i++)
	{
		for (int j = 0; j < 4; j++)
		{
			if (i == j)
			{
				mat[i][j] = a;
			} else
			{
				mat[i][j] = 0;
			}
		}
	}
}

Matrix::Matrix(float a, float b, float c, float d, float e, float f, float g, float h, float i, float j, float k, float l, float m, float n, float o, float p)
{
	mat[0][0] = a;
	mat[1][0] = b;
	mat[2][0] = c;
	mat[3][0] = d;
	mat[0][1] = e;
	mat[1][1] = f;
	mat[2][1] = g;
	mat[3][1] = h;
	mat[0][2] = i;
	mat[1][2] = j;
	mat[2][2] = k;
	mat[3][2] = l;
	mat[0][3] = m;
	mat[1][3] = n;
	mat[2][3] = o;
	mat[3][3] = p;
}

Matrix::Matrix(const Matrix3& m)
{
	mat[0][0] = m.mat[0][0];
	mat[1][0] = m.mat[1][0];
	mat[2][0] = m.mat[2][0];
	mat[3][0] = 0;
	mat[0][1] = m.mat[0][1];
	mat[1][1] = m.mat[1][1];
	mat[2][1] = m.mat[2][1];
	mat[3][1] = 0;
	mat[0][2] = m.mat[0][2];
	mat[1][2] = m.mat[1][2];
	mat[2][2] = m.mat[2][2];
	mat[3][2] = 0;
	mat[0][3] = 0;
	mat[1][3] = 0;
	mat[2][3] = 0;
	mat[3][3] = 1;
}

Matrix Matrix::operator+(const Matrix& m) const
{
	Matrix result(0.0);
	for (int row = 0; row < 4; row++)
	{
		for (int col = 0; col < 4; col++)
		{
			result.mat[col][row] = this->mat[col][row] + m.mat[col][row];
		}
	}
	return result;
}

Matrix Matrix::operator-(const Matrix& m) const
{
	Matrix result(0.0);
	for (int row = 0; row < 4; row++)
	{
		for (int col = 0; col < 4; col++)
		{
			result.mat[col][row] = this->mat[col][row] - m.mat[col][row];
		}
	}
	return result;
}

Matrix Matrix::operator*(const float& scalar) const
{
	Matrix result(0.0);
	for (int row = 0; row < 4; row++)
	{
		for (int col = 0; col < 4; col++)
		{
			result.mat[col][row] = this->mat[col][row] * scalar;
		}
	}
	return result;
}

Matrix Matrix::operator*(const Matrix& multiplier) const
{
	Vector4 srcA0(this->mat[0][0], this->mat[0][1], this->mat[0][2], this->mat[0][3]);
	Vector4 srcA1(this->mat[1][0], this->mat[1][1], this->mat[1][2], this->mat[1][3]);
	Vector4 srcA2(this->mat[2][0], this->mat[2][1], this->mat[2][2], this->mat[2][3]);
	Vector4 srcA3(this->mat[3][0], this->mat[3][1], this->mat[3][2], this->mat[3][3]);

	Vector4 srcB0(multiplier.mat[0][0], multiplier.mat[0][1], multiplier.mat[0][2], multiplier.mat[0][3]);
	Vector4 srcB1(multiplier.mat[1][0], multiplier.mat[1][1], multiplier.mat[1][2], multiplier.mat[1][3]);
	Vector4 srcB2(multiplier.mat[2][0], multiplier.mat[2][1], multiplier.mat[2][2], multiplier.mat[2][3]);
	Vector4 srcB3(multiplier.mat[3][0], multiplier.mat[3][1], multiplier.mat[3][2], multiplier.mat[3][3]);

	Vector4 result0 = srcA0 * srcB0.w + srcA1 * srcB0.x + srcA2 * srcB0.y + srcA3 * srcB0.z;
	Vector4 result1 = srcA0 * srcB1.w + srcA1 * srcB1.x + srcA2 * srcB1.y + srcA3 * srcB1.z;
	Vector4 result2 = srcA0 * srcB2.w + srcA1 * srcB2.x + srcA2 * srcB2.y + srcA3 * srcB2.z;
	Vector4 result3 = srcA0 * srcB3.w + srcA1 * srcB3.x + srcA2 * srcB3.y + srcA3 * srcB3.z;

	return Matrix(
		result0.w, result1.w, result2.w, result3.w,
		result0.x, result1.x, result2.x, result3.x,
		result0.y, result1.y, result2.y, result3.y,
		result0.z, result1.z, result2.z, result3.z);
}

Vector Matrix::operator*(const Vector& v) const
{
	return Vector(this->mat[0][0] * v.x + this->mat[1][0] * v.y + this->mat[2][0] * v.z + this->mat[3][0] * 0,
		this->mat[0][1] * v.x + this->mat[1][1] * v.y + this->mat[2][1] * v.z + this->mat[3][1] * 0,
		this->mat[0][2] * v.x + this->mat[1][2] * v.y + this->mat[2][2] * v.z + this->mat[3][2] * 0);
}

Vector Matrix::operator*(const Normal& v) const
{
	return Normal(this->mat[0][0] * v.x + this->mat[1][0] * v.y + this->mat[2][0] * v.z + this->mat[3][0] * 0,
		this->mat[0][1] * v.x + this->mat[1][1] * v.y + this->mat[2][1] * v.z + this->mat[3][1] * 0,
		this->mat[0][2] * v.x + this->mat[1][2] * v.y + this->mat[2][2] * v.z + this->mat[3][2] * 0);
}

Matrix Matrix::operator/(const float& scalar) const
{
	Matrix result(0.0);
	for (int row = 0; row < 4; row++)
	{
		for (int col = 0; col < 4; col++)
		{
			result.mat[col][row] = this->mat[col][row] / scalar;
		}
	}
	return result;
}

void Matrix::print() const
{
	for (int i = 0; i < 4; i++)
	{
		for (int j = 0; j < 4; j++)
		{
			printf("%f\t", mat[j][i]);			// column-major so iterate through i gets u thingsi n the same row
		}
		printf("\n");
	}
	printf("\n");
}

/* LocalGeo */

LocalGeo::LocalGeo()
{
	position = Point();
	normal = Normal();
}

/* Transformation */

Transformation::Transformation()
{
	this->matrix = Matrix();
	this->minvt = MathMethods::inverse(matrix);
}

Transformation::Transformation(const Matrix& m)
{
	this->matrix = m;
	this->minvt = MathMethods::inverse(m);
}

Transformation Transformation::identityTransformation()
{
	Transformation result;
	result.matrix = Matrix(1.0);
	result.minvt = Matrix(1.0);
	return result;
}

Point Transformation::operator* (const Point& p) const
{
	float x = matrix.mat[0][0] * p.x + matrix.mat[1][0] * p.y + matrix.mat[2][0] * p.z + matrix.mat[3][0] * 1.0;
	float y = matrix.mat[0][1] * p.x + matrix.mat[1][1] * p.y + matrix.mat[2][1] * p.z + matrix.mat[3][1] * 1.0;
	float z = matrix.mat[0][2] * p.x + matrix.mat[1][2] * p.y + matrix.mat[2][2] * p.z + matrix.mat[3][2] * 1.0;
	return Point(x, y, z);
}

Vector Transformation::operator* (const Vector& v) const
{
	float x = matrix.mat[0][0] * v.x + matrix.mat[1][0] * v.y + matrix.mat[2][0] * v.z + matrix.mat[3][0] * 0.0;
	float y = matrix.mat[0][1] * v.x + matrix.mat[1][1] * v.y + matrix.mat[2][1] * v.z + matrix.mat[3][1] * 0.0;
	float z = matrix.mat[0][2] * v.x + matrix.mat[1][2] * v.y + matrix.mat[2][2] * v.z + matrix.mat[3][2] * 0.0;
	return Vector(x, y, z);
}

Vector Transformation::operator* (const Normal& n) const
{
	return (this->matrix * n);
}

Ray Transformation::operator* (const Ray& r) const
{
	return Ray((*this) * r.position, (*this) * r.direction, r.t_min, r.t_max);
}

LocalGeo Transformation::operator* (const LocalGeo& lg) const
{
	LocalGeo result;
	result.position = (*this) * lg.position;
	result.normal = inverseTranspose() * lg.normal;
	return result;
}

void Transformation::setMatrix(const Matrix& m)
{
	this->matrix = m;
	this->minvt = MathMethods::inverse(m);
}

Transformation Transformation::inverseTranspose() const
{
	Matrix invTranMatrix = MathMethods::inverseTranspose(matrix);
	Matrix invInvTranMatrix = MathMethods::inverse(invTranMatrix);

	Transformation result;
	result.matrix = invTranMatrix;
	result.minvt = invInvTranMatrix;
	return result;
}

Transformation Transformation::inverse() const
{
	Transformation result;
	result.matrix = this->minvt;
	result.minvt = this->matrix;
	return result;
}

void Transformation::print() const
{
	matrix.print();
}

/* Color */

Color::Color()
{
	r = 0;
	g = 0;
	b = 0;
}

Color::Color(float r, float g, float b)
{
	this->r = r;
	this->g = g;
	this->b = b;
}

Color Color::operator+(const Color& c)  const
{
	return Color(this->r + c.r, this->g + c.g, this->b + c.b);
}

Color Color::operator - (const Color& c) const
{
	return Color(this->r - c.r, this->g - c.g, this->b - c.b);
}

Color Color::operator*(const Color& c) const
{
	return Color(this->r * c.r, this->g * c.g, this->b * c.b);
}

Color Color::add(const float& x, const float& y, const float& z) const
{
	Color color(x, y, z);
	return (*this) + color;
}

Color Color::subtract(const float& x, const float& y, const float& z) const
{
	Color color(x, y, z);
	return (*this) - color;
}

Color Color::operator*(const float& n) const
{
	return Color(r * n, g * n, b * n);
}

Color Color::operator/(const float& n) const
{
	return Color(r / n, g / n, b / n);
}

/* BRDF */

BRDF::BRDF()
{
	BRDF(Color(0.2, 0.2, 0.2), Color(0, 0, 0), Color(0, 0, 0), Color(0, 0, 0), Color(0, 0, 0), 1.0, 1.0);
}

BRDF::BRDF(Color ambient, Color diffuse, Color specular, Color emissivity, Color transparency, float shininess, float refraction)
{
	this->COMPONENT_AMBIENT = ambient;
	this->COMPONENT_DIFFUSE = diffuse;
	this->COMPONENT_SPECULAR = specular;
	this->COMPONENT_EMISSIVITY = emissivity;
	this->COMPONENT_TRANSPARENCY = transparency;
	this->SHININESS = shininess;
	this->INDEX_REFRACTION = refraction;
}

/* Sample */

Sample::Sample()
{
	x = 0;
	y = 0;
}

Sample::Sample(float x, float y)
{
	this->x = x;
	this->y = y;
}

/* Vector4 */
Vector4::Vector4()
{
	w = 0.0;
	x = 0.0;
	y = 0.0;
	z = 0.0;
}

Vector4::Vector4(float w, float x, float y, float z)
{
	this->w = w;
	this->x = x;
	this->y = y;
	this->z = z;
}

Vector4 Vector4::operator+(const Vector4& v)
{
	return Vector4(w + v.w, x + v.x, y + v.y, z + v.z);
}

Vector4 Vector4::operator-(const Vector4& v)
{
	return Vector4(w - v.w, x - v.x, y - v.y, z - v.z);
}

Vector4 Vector4::operator*(float scalar)
{
	return Vector4(w * scalar, x * scalar, y * scalar, z * scalar);
}

Vector4 Vector4::operator*(const Vector4& v)
{
	return Vector4(this->w * v.w, this->x * v.x, this->y * v.y, this->z * v.z);
}

Vector4 Vector4::operator/(float scalar)
{
	return Vector4(w / scalar, x / scalar, y / scalar, z / scalar);
}

float Vector4::dot(const Vector4& v)
{
	return this->w * v.w + this->x * v.x + this->y * v.y + this->z * v.z;
}

/* Matrix 3x3 */
Matrix3::Matrix3()
{
	Matrix3(1.0);
}

Matrix3::Matrix3(float a)
{
	for (int i = 0; i < 3; i++)
	{
		for (int j = 0; j < 3; j++)
		{
			if (i == j)
			{
				mat[i][j] = a;
			}
			else
			{
				mat[i][j] = 0;
			}
		}
	}
}

Matrix3::Matrix3(float a, float b, float c, float d, float e, float f, float g, float h, float i)
{
	mat[0][0] = a;
	mat[1][0] = b;
	mat[2][0] = c;
	mat[0][1] = d;
	mat[1][1] = e;
	mat[2][1] = f;
	mat[0][2] = g;
	mat[1][2] = h;
	mat[2][2] = i;
}

Matrix3 Matrix3::operator+(const Matrix3& m) const
{
	Matrix3 result(0.0);
	for (int row = 0; row < 3; row++)
	{
		for (int col = 0; col < 3; col++)
		{
			result.mat[col][row] = this->mat[col][row] + m.mat[col][row];
		}
	}
	return result;
}

Matrix3 Matrix3::operator-(const Matrix3& m) const
{
	Matrix3 result(0.0);
	for (int row = 0; row < 3; row++)
	{
		for (int col = 0; col < 3; col++)
		{
			result.mat[col][row] = this->mat[col][row] - m.mat[col][row];
		}
	}
	return result;
}

Matrix3 Matrix3::operator*(const float& scalar) const
{
	Matrix3 result(0.0);
	for (int row = 0; row < 3; row++)
	{
		for (int col = 0; col < 3; col++)
		{
			result.mat[col][row] = this->mat[col][row] * scalar;
		}
	}
	return result;
}

Matrix3 Matrix3::operator*(const Matrix3& multiplier) const
{
	Vector srcA0(this->mat[0][0], this->mat[1][0], this->mat[2][0]);
	Vector srcA1(this->mat[0][1], this->mat[1][1], this->mat[2][1]);
	Vector srcA2(this->mat[0][2], this->mat[1][2], this->mat[2][2]);

	Vector srcB0(multiplier.mat[0][0], multiplier.mat[1][0], multiplier.mat[2][0]);
	Vector srcB1(multiplier.mat[0][1], multiplier.mat[1][1], multiplier.mat[2][1]);
	Vector srcB2(multiplier.mat[0][2], multiplier.mat[1][2], multiplier.mat[2][2]);

	Vector result0 = srcA0 * srcB0.x + srcA1 * srcB0.y + srcA2 * srcB0.z;
	Vector result1 = srcA0 * srcB1.x + srcA1 * srcB1.y + srcA2 * srcB1.z;
	Vector result2 = srcA0 * srcB2.x + srcA1 * srcB2.y + srcA2 * srcB2.z;

	return Matrix3(result0.x, result0.y, result0.z,
		result1.x, result1.y, result1.z,
		result2.x, result2.y, result2.z);
}

Vector Matrix3::operator*(const Vector& v) const
{
	return Vector(this->mat[0][0] * v.x + this->mat[1][0] * v.y + this->mat[2][0] * v.z,
		this->mat[0][1] * v.x + this->mat[1][1] * v.y + this->mat[2][1] * v.z,
		this->mat[0][2] * v.x + this->mat[1][2] * v.y + this->mat[2][2] * v.z);
}

Vector Matrix3::operator*(const Normal& v) const
{
	return Vector(this->mat[0][0] * v.x + this->mat[1][0] * v.y + this->mat[2][0] * v.z,
		this->mat[0][1] * v.x + this->mat[1][1] * v.y + this->mat[2][1] * v.z,
		this->mat[0][2] * v.x + this->mat[1][2] * v.y + this->mat[2][2] * v.z);
}

void Matrix3::print() const
{

}

Matrix3 Matrix3::operator/(const float& scalar) const
{
	Matrix3 result(0.0);
	for (int row = 0; row < 3; row++)
	{
		for (int col = 0; col < 3; col++)
		{
			result.mat[col][row] = this->mat[col][row] / scalar;
		}
	}
	return result;
}

