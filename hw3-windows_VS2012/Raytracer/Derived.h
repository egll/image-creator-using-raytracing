#ifndef EAGLE_RAYTRACE_DERIVED
#define EAGLE_RAYTRACE_DERIVED

#include "Basic.h"
#include <FreeImage.h>
#include <string>
#include <vector>
#include <fstream>
#include <stack>

struct Shape;
struct Primitive;
struct Intersection;
struct GeometricPrimitive;
struct AggregatePrimitive;
struct Material;
struct Sampler;
struct Camera;
struct RayTracer;
struct Light;
struct Film;
struct Scene;

extern Scene* scene;

struct Shape
{
	virtual ~Shape() {}
	int objectID;
	virtual bool intersect(Ray& ray, float* t_hit, LocalGeo* local){ return false; }
	virtual bool intersectP(Ray& ray){ return false; }
};

struct Sphere : public Shape
{
	float radius;
	Point center;
	Sphere();
	Sphere(float x, float y, float z, float r, int id);
	virtual bool intersect(Ray& ray, float* t_hit, LocalGeo* local);
	virtual bool intersectP(Ray& ray);
};

struct Triangle : public Shape
{
	Point a;
	Point b;
	Point c;
	Triangle(Point v0, Point v1, Point v2, int id);
	Triangle(float v0x, float v0y, float v0z, float v1x, float v1y, float v1z, float v2x, float v2y, float v2z, int id);
	virtual bool intersect(Ray& ray, float* t_hit, LocalGeo* local);
	virtual bool intersectP(Ray& ray);
};

struct Plane : public Shape
{
	Point a;
	Point b;
	Point c;
	Plane(Point v0, Point v1, Point v2);
	virtual bool intersect(Ray& ray, float* t_hit, LocalGeo* local);
	virtual bool intersectP(Ray& ray);
};

struct Cylinder : public Shape
{
	bool bounded;
	Cylinder();
	Cylinder(bool bounded);
	virtual bool intersect(Ray& ray, float* t_hit, LocalGeo* local);
	virtual bool intersectP(Ray& ray);
};

struct Primitive
{
	virtual ~Primitive() {}
	virtual bool intersect(Ray& ray, float* thit, Intersection* in) { return false; }
	virtual bool intersectP(Ray& ray) { return false; }
	virtual void getBRDF(LocalGeo& local, BRDF* brdf) {}
};

struct Intersection
{
	LocalGeo localGeo;
	Primitive* primitive;
};

struct GeometricPrimitive : public Primitive
{
	Transformation objToWorld, worldToObj;
	Shape *shape;
	BRDF brdf;						//will me Material* mat later (which holds brdf), but for now, just brdf
	GeometricPrimitive(Shape *shape, Transformation transformation, BRDF brdf);
	GeometricPrimitive(Shape *shape, float tx, float ty, float tz, float sx, float sy, float sz, float rotx, float roty, float rotz, float angle, float kar, float kag, float  kab, float kdr, float kdg, float kdb, float ksr, float ksg, float ksb, float p);
	GeometricPrimitive(Shape *shape, float tx, float ty, float tz, float sx, float sy, float sz, float rotx, float roty, float rotz, float kar, float kag, float  kab, float kdr, float kdg, float kdb, float ksr, float ksg, float ksb, float p);
	virtual bool intersect(Ray& ray, float* t_hit, Intersection* intersection);
	virtual bool intersectP(Ray& ray);
	virtual void getBRDF(LocalGeo& local, BRDF* brdf);
};

struct AggregatePrimitive;
struct Material
{
	BRDF constantBRDF;
	BRDF getBRDF(LocalGeo& local, BRDF* brdf);
};

struct Sampler
{
	int width, height, currentX, currentY;
	bool done;
	Sampler();
	Sampler(int width, int height);
	bool generateSample(Sample* sample);
};

struct Camera
{
	Point lookat;
	Point lookfrom;
	Vector up;
	float fov;

	Vector x;
	Vector y;
	Vector z;
	Point upleft, upright, downleft, downright;
	Vector XInc, YInc;
	Camera();
	Camera::Camera(float lookfromX, float lookfromY, float lookfromZ, float lookatX, float lookatY, float lookatZ, float upX, float upY, float upZ, float fov);
	void generateRay(Sample& sample, Ray *ray);
};

struct RayTracer
{
	int maxDepth;
	RayTracer();
	RayTracer(int maxDepth);
	void trace(Ray& ray, int depth, Color* color);
};

struct Light
{
	virtual ~Light() {}
	Color color;
	Vector direction;
	Point position;

	enum LightType {directional, point};
	LightType type;
	virtual void generateLightRay(LocalGeo& local, Ray* ray, Color* color){ return; }
};

struct DirectionalLight : public Light
{
	DirectionalLight(float x, float y, float z, float r, float g, float b);
	virtual void generateLightRay(LocalGeo& local, Ray* ray, Color* color);
};

struct PointLight : public Light
{
	PointLight(float x, float y, float z, float r, float g, float b);
	virtual void generateLightRay(LocalGeo& local, Ray* ray, Color* color);
};

struct Film
{
	FIBITMAP* bitmap;
	const char* outputFilename;
	Film();
	Film(int width, int height, const char* outputFilename);
	void commit(Sample& sample, Color& color);
	void writeImage();
};

struct Scene
{
	float width, height;
	std::string outputName;
	float lookfromx, lookfromy, lookfromz, lookatx, lookaty, lookatz, upx, upy, upz, fov;
	int maxDepth;
	float attenuation[3];

	RayTracer raytracer;
	Film film;
	Camera camera;
	Sampler sampler;
	Sample sample;
	Ray ray;
	Color color;
	std::vector<Shape *> shapes;
	std::vector<Primitive *> primitives;
	std::vector<Light *> lights;
	Scene();
	Scene(char* filename);
	void initialize();
	void render();
	void loadScene(std::string filename);
};

#endif