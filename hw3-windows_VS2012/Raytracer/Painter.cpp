#include "Derived.h"
#include "MathMethods.h"

Sampler::Sampler()
{
}

Sampler::Sampler(int width, int height)
{
	this->width = width;
	this->height = height;
	this->currentX = 0;
	this->currentY = 0;
	this->done = false;
}

bool Sampler::generateSample(Sample* sample)
{
	if (done)
	{
		return done;
	}
	else
	{
		sample->x = currentX;
		sample->y = currentY;
		if (currentX < width - 1)
		{
			currentX++;
		}
		else if (currentX == width - 1)
		{
			if (currentY < height - 1)
			{
				currentY++;
				currentX = 0;
			}
			else
			{
				done = true;
			}
		}
		return false;
	}
}

/* Film */

Film::Film()
{
	FreeImage_Initialise();
	this->outputFilename = "output.png";
	this->bitmap = FreeImage_Allocate(600, 600, 24);
}

Film::Film(int width, int height, const char* file)
{
	FreeImage_Initialise();
	this->outputFilename = file;
	this->bitmap = FreeImage_Allocate(width, height, 24);
}

void Film::commit(Sample& sample, Color& color)
{
	RGBQUAD FreeImageColor;
	if (color.r > 1.0)
	{
		color.r = 1.0;
	}

	if (color.g > 1.0)
	{
		color.g = 1.0;
	}

	if (color.b > 1.0)
	{
		color.b = 1.0;
	}

	FreeImageColor.rgbRed = color.r * 255.0;
	FreeImageColor.rgbGreen = color.g * 255.0;
	FreeImageColor.rgbBlue = color.b * 255.0;

	FreeImage_SetPixelColor(bitmap, sample.x, sample.y, &FreeImageColor);
}

void Film::writeImage()
{
	if (FreeImage_Save(FIF_PNG, bitmap, outputFilename, 0))
	{
		std::printf("Image '%s' saved as png", outputFilename);
	}
	FreeImage_DeInitialise();
}

/* Camera */

Camera::Camera()
{
}

Camera::Camera(float lookfromX, float lookfromY, float lookfromZ, float lookatX, float lookatY, float lookatZ, float upX, float upY, float upZ, float fov)
{
	this->lookfrom.x = lookfromX;
	this->lookfrom.y = lookfromY;
	this->lookfrom.z = lookfromZ;

	this->lookat.x = lookatX;
	this->lookat.y = lookatY;
	this->lookat.z = lookatZ;

	this->up.x = upX;
	this->up.y = upY;
	this->up.z = upZ;

	this->fov = fov;

	/* CHECK FOR LATER */
	float h = 2 * tan(MathMethods::radians(fov / 2));
	float aspectRatio = scene->width / scene->height;
	float w = h * aspectRatio;

	this->z = (lookat - lookfrom).normalize();
	this->x = MathMethods::cross(z, up).normalize();
	this->y = MathMethods::cross(x, z).normalize();

	/* CHECK FOR LATER */
	Point center = lookfrom + z;
	this->upleft = center + (y * h / 2) - x * (w / 2);
	this->upright = center + (y * h / 2) + x * (w / 2);
	this->downleft = center - (y * h / 2) - x * (w / 2);
	this->downright = center - (y * h / 2) + x * (w / 2);

	/* CHECK FOR LATER */
	this->XInc = x * w / scene->width;
	this->YInc = y * h / scene->height;
}

void Camera::generateRay(Sample& sample, Ray* ray)
{
	/* CHECK FOR LATER */
	ray->position = lookfrom;
	ray->direction = YInc * (sample.y - scene->height / 2.0 + 0.5) + z + XInc * (sample.x - scene->width / 2.0 + 0.5);
	ray->t_min = 1.0;
	ray->t_max = 100000.0;
}

/* Lights */

DirectionalLight::DirectionalLight(float x, float y, float z, float r, float g, float b)
{
	this->direction.x = x;
	this->direction.y = y;
	this->direction.z = z;

	this->position.x = 0.0;
	this->position.y = 0.0;
	this->position.z = 0.0;

	this->color.r = r;
	this->color.g = g;
	this->color.b = b;

	this->type = LightType::directional;
}

void DirectionalLight::generateLightRay(LocalGeo& local, Ray* ray, Color* color)
{
	ray->position = local.position;
	ray->direction = MathMethods::oppositeDirection(this->direction); //going to the opposite direction of the camera
	ray->t_min = 0.001;
	ray->t_max = 9999;
	*color = this->color;
}

PointLight::PointLight(float x, float y, float z, float r, float g, float b)
{
	this->direction.x = 0.0;
	this->direction.y = 0.0;
	this->direction.z = 0.0;

	this->position.x = x;
	this->position.y = y;
	this->position.z = z;

	this->color.r = r;
	this->color.g = g;
	this->color.b = b;

	this->type = LightType::point;
}

void PointLight::generateLightRay(LocalGeo& local, Ray* ray, Color* color)
{
	ray->position = local.position;
	ray->direction = this->position - local.position;
	ray->t_min = 0.0001;
	ray->t_max = 1;
	*color = this->color;
}