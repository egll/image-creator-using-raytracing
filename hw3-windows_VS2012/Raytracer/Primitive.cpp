#include "Derived.h"
#include "MathMethods.h"

GeometricPrimitive::GeometricPrimitive(Shape* shape, Transformation transformation, BRDF brdf)
{
	this->objToWorld = transformation;
	this->worldToObj = transformation.inverse();
	this->shape = shape;
	this->brdf = brdf;
}

GeometricPrimitive::GeometricPrimitive(Shape *shape, float tx, float ty, float tz, float sx, float sy, float sz, float rotx, float roty, float rotz, float kar, float kag, float  kab, float kdr, float kdg, float kdb, float ksr, float ksg, float ksb, float p)
{
	GeometricPrimitive(shape, tx, ty, tx, sx, sy, sz, rotx, roty, rotz, 0.0, kar, kag, kab, kdr, kdg, kdb, ksr, ksg, ksb, p);
}

GeometricPrimitive::GeometricPrimitive(Shape *shape, float tx, float ty, float tz, float sx, float sy, float sz, float rotx, float roty, float rotz, float angle, float kar, float kag, float  kab, float kdr, float kdg, float kdb, float ksr, float ksg, float ksb, float p)
{
	Matrix rotate = MathMethods::rotation(rotx, roty, rotz, angle);
	Matrix scale = MathMethods::scaling(sx, sy, sz);
	Matrix translate = MathMethods::translation(tx, ty, tz);

	BRDF brdf;
	brdf.COMPONENT_AMBIENT.r = kar;
	brdf.COMPONENT_AMBIENT.g = kag;
	brdf.COMPONENT_AMBIENT.b = kab;
	brdf.COMPONENT_DIFFUSE.r = kdr;
	brdf.COMPONENT_DIFFUSE.g = kdg;
	brdf.COMPONENT_DIFFUSE.b = kdb;
	brdf.COMPONENT_SPECULAR.r = ksr;
	brdf.COMPONENT_SPECULAR.g = ksg;
	brdf.COMPONENT_SPECULAR.b = ksb;
	brdf.SHININESS = p;

	this->objToWorld.matrix = translate * rotate * scale;
	this->worldToObj = MathMethods::inverse(this->objToWorld.matrix);
	this->shape = shape;
	this->brdf = brdf;
}

bool GeometricPrimitive::intersect(Ray& ray, float* t_hit, Intersection* intersection)
{
	Ray rayToObject = worldToObj * ray;
	float t_hitObject;
	LocalGeo localGeoObject;
	if (shape->intersect(rayToObject, &t_hitObject, &localGeoObject))
	{
		*t_hit = t_hitObject;
		intersection->localGeo = objToWorld * localGeoObject;
		intersection->primitive = this;
		return true;
	}
	else
	{
		return false;
	}
}

bool GeometricPrimitive::intersectP(Ray& ray)
{
	Ray rayToObject = worldToObj * ray;
	float t_hitObject;
	LocalGeo localGeoObject;
	if (shape->intersect(rayToObject, &t_hitObject, &localGeoObject))
	{
		return true;
	}
	else
	{
		return false;
	}
}

void GeometricPrimitive::getBRDF(LocalGeo& local, BRDF* brdf)
{
	*brdf = this->brdf;
}