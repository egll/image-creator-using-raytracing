#include "Derived.h"
#include "IOMethods.h"
#include "MathMethods.h"
#include <iostream>
#include <sstream>

Scene::Scene(){
	width = 800;
	height = 600;
	maxDepth = 5;
	lookfromx = -10;
	lookfromy = 0;
	lookfromz = 0;
	lookatx = 0;
	lookaty = 0;
	lookatz = 0;
	upx = 0;
	upy = 1;
	upz = 0;
	fov = 100;
	outputName = "test.png";
	attenuation[0] = 1.0;
	attenuation[1] = 0.0;
	attenuation[2] = 0.0;
}

Scene::Scene(char* filename)
{
	attenuation[0] = 1.0;
	attenuation[1] = 0.0;
	attenuation[2] = 0.0;
	std::string* file = new std::string(filename);
	this->loadScene(*file);
	delete(file);
}

void Scene::initialize()
{
	this->raytracer = *new RayTracer(maxDepth);
	this->sampler = *new Sampler(width, height);
	this->film = *new Film(width, height, outputName.c_str());
	this->camera = *new Camera(lookfromx, lookfromy, lookfromz, lookatx, lookaty, lookatz, upx, upy, upz, fov);
}

void Scene::render()
{
	float totalPixels = width * height;
	float pixelsDone = 0;

	int percentageProcessed;
	int previousPercentageProcessed = -1;

	std::printf("Painting scene \n");
	while (!sampler.generateSample(&sample))
	{
		camera.generateRay(sample, &ray);
		raytracer.trace(ray, 1, &color);
		film.commit(sample, color);

		percentageProcessed = 100 * ++pixelsDone / totalPixels;
		if (percentageProcessed != previousPercentageProcessed)
		{
			std::printf("%d%% ", percentageProcessed);
			previousPercentageProcessed = percentageProcessed;
		}
		
	}
	std::printf("\n");
	film.writeImage();
}

void Scene::loadScene(std::string filename)
{
	outputName = "output.png";
	std::ifstream input_file;
	input_file.open(filename.c_str());
	if (!input_file.is_open())
	{
		std::cerr << "Unable to Open Input Data File " << filename << "\n";
		throw 2;
	}
	else
	{
		std::string line, command;
		std::stack<Transformation> transfstack;
		Transformation currentTransformation = Transformation::identityTransformation();
		BRDF currentBRDF;
		std::vector<Point> vertices;
		maxDepth = 5;

		getline(input_file, line);
		while (input_file)
		{
			if ((line.find_first_not_of(" \t\r\n") != std::string::npos) && (line[0] != '#'))
			{
				// Ruled out comment and blank lines 

				std::stringstream s(line);
				s >> command;
				float values[10]; // Position and color for light, colors for others
				std::string stringValues;
				// Up to 10 params for cameras.  
				bool validinput; // Validity of input

				if (command == "size") {
					validinput = IOMethods::readValues(s, 2, values);
					if (validinput)
					{
						width = (int)values[0];
						height = (int)values[1];
					}
				}
				else if (command == "maxdepth")
				{
					validinput = IOMethods::readValues(s, 1, values);
					if (validinput)
					{
						maxDepth = (int)values[0];
					}
				}
				else if (command == "output")
				{
					s >> outputName;
				}
				else if (command == "camera")
				{
					validinput = IOMethods::readValues(s, 10, values);
					if (validinput)
					{
						lookfromx = values[0];
						lookfromy = values[1];
						lookfromz = values[2];

						lookatx = values[3];
						lookaty = values[4];
						lookatz = values[5];

						upx = values[6];
						upy = values[7];
						upz = values[8];

						fov = values[9];
					}
				}
				else if (command == "sphere")
				{
					validinput = IOMethods::readValues(s, 4, values);
					if (validinput)
					{
						Sphere* sphere = new Sphere((float)values[0], (float)values[1], (float)values[2], (float)values[3], primitives.size() + 1);
						GeometricPrimitive* geoPrimitive = new GeometricPrimitive(sphere, currentTransformation, currentBRDF);
						primitives.push_back(geoPrimitive);
					}
				}
				else if (command == "maxverts")
				{
					//Max Vertex
				}
				else if (command == "maxvertnorms")
				{
					//Max Normal
				}
				else if (command == "vertex")
				{
					validinput = IOMethods::readValues(s, 3, values);
					if (validinput)
					{
						Point vector((float)values[0], (float)values[1], (float)values[2]);
						vertices.push_back(vector);
					}
				}
				else if (command == "vertexnormal")
				{
					//TO DO
				}
				else if (command == "tri")
				{
					validinput = IOMethods::readValues(s, 3, values);
					if (validinput)
					{
						Point vector1 = vertices.at(values[0]);
						Point vector2 = vertices.at(values[1]);
						Point vector3 = vertices.at(values[2]);

						Triangle* triangle = new Triangle(vector1, vector2, vector3, primitives.size() + 1);
						GeometricPrimitive* geoPrimitive = new GeometricPrimitive(triangle, currentTransformation, currentBRDF);
						primitives.push_back(geoPrimitive);
					}
				}
				else if (command == "trinormal")
				{
					//TO DO
				}
				else if (command == "translate")
				{
					validinput = IOMethods::readValues(s, 3, values);
					if (validinput)
					{
						currentTransformation.setMatrix(currentTransformation.matrix * MathMethods::translation((float)values[0], (float)values[1], (float)values[2]));
					}
				}
				else if (command == "rotate")
				{
					validinput = IOMethods::readValues(s, 4, values);
					if (validinput)
					{
						currentTransformation.setMatrix(currentTransformation.matrix * MathMethods::rotation((float)values[0], (float)values[1], (float)values[2], (float)values[3]));
					}
				}
				else if (command == "scale")
				{
					validinput = IOMethods::readValues(s, 3, values);
					if (validinput)
					{
						currentTransformation.setMatrix(currentTransformation.matrix * MathMethods::scaling((float)values[0], (float)values[1], (float)values[2]));
					}
				}
				else if (command == "pushTransform")
				{
					transfstack.push(currentTransformation);
				}
				else if (command == "popTransform")
				{
					currentTransformation = transfstack.top();
					transfstack.pop();
				}
				else if (command == "directional")
				{
					validinput = IOMethods::readValues(s, 6, values);
					if (validinput)
					{
						DirectionalLight* light = new DirectionalLight(-(float)values[0], -(float)values[1], -(float)values[2], (float)values[3], (float)values[4], (float)values[5]);
						lights.push_back(light);
					}
				}
				else if (command == "point")
				{
					validinput = IOMethods::readValues(s, 6, values);
					if (validinput)
					{
						PointLight* light = new PointLight((float)values[0], (float)values[1], (float)values[2], (float)values[3], (float)values[4], (float)values[5]);
						lights.push_back(light);
					}
				}
				else if (command == "ambient")
				{
					validinput = IOMethods::readValues(s, 3, values);
					if (validinput)
					{
						currentBRDF.COMPONENT_AMBIENT = Color((float)values[0], (float)values[1], (float)values[2]);
					}
				}
				else if (command == "diffuse")
				{
					validinput = IOMethods::readValues(s, 3, values);
					if (validinput)
					{
						currentBRDF.COMPONENT_DIFFUSE = Color((float)values[0], (float)values[1], (float)values[2]);
					}
				}
				else if (command == "specular")
				{
					validinput = IOMethods::readValues(s, 3, values);
					if (validinput)
					{
						currentBRDF.COMPONENT_SPECULAR = Color((float)values[0], (float)values[1], (float)values[2]);
					}
				}
				else if (command == "emission")
				{
					validinput = IOMethods::readValues(s, 3, values);
					if (validinput)
					{
						currentBRDF.COMPONENT_EMISSIVITY = Color((float)values[0], (float)values[1], (float)values[2]);
					}
				}
				else if (command == "refraction")
				{
					validinput = IOMethods::readValues(s, 4, values);
					if (validinput)
					{
						currentBRDF.COMPONENT_TRANSPARENCY = Color((float)values[0], (float)values[1], (float)values[2]);
						currentBRDF.INDEX_REFRACTION = (float)values[3];
					}
				}
				else if (command == "shininess")
				{
					validinput = IOMethods::readValues(s, 1, values);
					if (validinput)
					{
						currentBRDF.SHININESS = (float)values[0];
					}
				}
				else if (command == "attenuation")
				{
					validinput = IOMethods::readValues(s, 3, values);
					if (validinput)
					{
						attenuation[0] = (float)values[0];
						attenuation[1] = (float)values[1];
						attenuation[2] = (float)values[2];
					}
				}
				else
				{
					std::cerr << "Unknown command: " << command << std::endl;
				}
			}
			std::getline(input_file, line);
		}

	}

	input_file.close();
}