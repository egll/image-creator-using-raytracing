#include "Derived.h"
#include "MathMethods.h"

/* Sphere */
Sphere::Sphere()
{
	center.x = 0;
	center.y = 0;
	center.z = 0;
	radius = 1;
}

Sphere::Sphere(float x, float y, float z, float r, int id)
{
	center.x = x;
	center.y = y;
	center.z = z;
	radius = r;
	objectID = id;
}

bool Sphere::intersect(Ray& ray, float* t_hit, LocalGeo* local)
{
	//Sphere-line intersection
	float equation_A = (ray.direction * ray.direction);
	float equation_B = 2 * ((ray.position - center) * ray.direction);
	float equation_C = (ray.position - center) * (ray.position - center) - (radius * radius);

		float determinant = equation_B * equation_B - 4 * equation_A * equation_C;
	
	if (determinant < 0)
		return false;

	float t0 = (-equation_B - sqrt(determinant)) / (2 * equation_A);
	float t1 = (-equation_B + sqrt(determinant)) / (2 * equation_A);

	bool ist0Valid = t0 > ray.t_min && t0 < ray.t_max;
	bool ist1Valid = t1 > ray.t_min && t1 < ray.t_max;

	if (ist0Valid)
	{
		*t_hit = t0;
	}
	else if (ist1Valid)
	{
		*t_hit = t1;
	}
	else
	{
		return false;
	}
	Vector hit = ray.pointAtTime(*t_hit) - center;
	Normal normal(hit);
	(*local).normal = normal;
	(*local).position = ray.pointAtTime(*t_hit);
	return true;
}

bool Sphere::intersectP(Ray& ray)
{
	//Sphere-line intersection
	float equation_A = (ray.direction * ray.direction);
	float equation_B = 2 * ((ray.position - center) * ray.direction);
	float equation_C = (ray.position - center) * (ray.position - center) - (radius * radius);

	float determinant = equation_B * equation_B - 4 * equation_A * equation_C;

	if (determinant < 0)
		return false;

	float t0 = (-equation_B - sqrt(determinant)) / (2 * equation_A);
	float t1 = (-equation_B + sqrt(determinant)) / (2 * equation_A);

	bool ist0Valid = t0 > ray.t_min && t0 < ray.t_max;
	bool ist1Valid = t1 > ray.t_min && t1 < ray.t_max;

	if (ist0Valid || ist1Valid)
	{
		return true;
	}
	return false;
}

/* Triangle */

Triangle::Triangle(Point v0, Point v1, Point v2, int id)
{
	a = v0;
	b = v1;
	c = v2;
	objectID = id;
}

Triangle::Triangle(float v0x, float v0y, float v0z, float v1x, float v1y, float v1z, float v2x, float v2y, float v2z, int id)
{
	Point p1(v0x, v0y, v0z);
	Point p2(v1x, v1y, v1z);
	Point p3(v2x, v2y, v2z);
	Triangle(p1, p2, p3, id);
}

bool Triangle::intersect(Ray& ray, float* t_hit, LocalGeo* local)
{
	Vector u = b - a;
	Vector v = c - a;
	Vector w = MathMethods::cross(u, v);
	Normal normal(w);
	if (normal * ray.direction == 0)
	{
		return false;
	}

	float r = normal * (a - ray.position) / (normal * ray.direction);
	if (r < ray.t_min || r > ray.t_max)
	{
		return false;
	}

	Point point = ray.pointAtTime(r);

	bool check1 = MathMethods::cross(b - a, point - a) * normal >= 0;
	bool check2 = MathMethods::cross(c - b, point - b) * normal >= 0;
	bool check3 = MathMethods::cross(a - c, point - c) * normal >= 0;
	if (!check1 || !check2 || !check3)
	{
		return false;
	}

	*t_hit = r;
	(*local).normal = normal;
	(*local).position = point;
	return true;
}

bool Triangle::intersectP(Ray& ray)
{
	Vector u = b - a;
	Vector v = c - a;
	Vector w = MathMethods::cross(u, v);
	Normal normal(w);
	if (normal * ray.direction == 0)
	{
		return false;
	}

	float r = normal * (a - ray.position) / (normal * ray.direction);
	if (r < ray.t_min || r > ray.t_max)
	{
		return false;
	}

	Point point = ray.pointAtTime(r);

	bool check1 = MathMethods::cross(b - a, point - a) * normal >= 0;
	bool check2 = MathMethods::cross(c - b, point - b) * normal >= 0;
	bool check3 = MathMethods::cross(a - c, point - c) * normal >= 0;
	if (!check1 || !check2 || !check3)
	{
		return false;
	}

	return true;
}

/* Plane */

Plane::Plane(Point v0, Point v1, Point v2)
{
	a = v0;
	b = v1;
	c = v2;
}

bool Plane::intersect(Ray& ray, float* t_hit, LocalGeo* local)
{
	Vector u = b - a;
	Vector v = c - a;
	Vector w = MathMethods::cross(u, v);
	Normal normal(w);

	float r = normal * (a - ray.position) / (normal * ray.direction);
	if (r < ray.t_min || r > ray.t_max)
	{
		return false;
	}

	Point point = ray.pointAtTime(r);									/* point that ray intersects the plane */

	*t_hit = r;
	local->normal = normal;
	local->position = point;
	return true;
}

bool Plane::intersectP(Ray& ray)
{
	Vector u = b - a;
	Vector v = c - a;
	Vector w = MathMethods::cross(u, v);
	Normal normal(w);

	float r = normal * (a - ray.position) / (normal * ray.direction);
	if (r < ray.t_min || r > ray.t_max)
	{
		return false;
	}
	//Point p = ray.pointAtTime(r);
	return true;
}

/* Cylinder */

Cylinder::Cylinder()
{
	this->bounded = false;
}

Cylinder::Cylinder(bool bounded)
{
	this->bounded = bounded;
}

bool Cylinder::intersect(Ray& ray, float* t_hit, LocalGeo* local)
{
	float equation_A = ray.direction.x * ray.direction.x + ray.direction.y * ray.direction.y;
	float equation_B = 2 * (ray.position.x * ray.direction.x + ray.position.y * ray.direction.y);
	float equation_C = ray.position.x * ray.position.x + ray.position.y * ray.position.y - 1;

	float discriminant = equation_B * equation_B + 4 * equation_A * equation_C;
	if (discriminant < 0) {
		return false;
	}
	float t0 = (-equation_B - sqrt(discriminant)) / (2 * equation_A);
	float t1 = (-equation_B + sqrt(discriminant)) / (2 * equation_A);

	bool ist0Valid = t0 > ray.t_min && t0 < ray.t_max && !(bounded && (ray.pointAtTime(t0).z > 1 || ray.pointAtTime(t0).z < -1));
	bool ist1Valid = t1 > ray.t_min && t1 < ray.t_max && !(bounded && (ray.pointAtTime(t1).z > 1 || ray.pointAtTime(t1).z < -1));

	if (ist0Valid)
	{
		*t_hit = t0;
	}
	else if (ist1Valid)
	{
		*t_hit = t1;
	}
	else
	{
		return false;
	}

	float hit = *t_hit;
	Point point = ray.pointAtTime(hit);
	Normal normal(point.x, point.y, 0);
	(*local).normal = normal;
	(*local).position = point;
	
	return true;
}

bool Cylinder::intersectP(Ray& ray)
{
	float equation_A = ray.direction.x * ray.direction.x + ray.direction.y * ray.direction.y;
	float equation_B = 2 * (ray.position.x * ray.direction.x + ray.position.y * ray.direction.y);
	float equation_C = ray.position.x * ray.position.x + ray.position.y * ray.position.y - 1;

	float discriminant = equation_B * equation_B + 4 * equation_A * equation_C;
	if (discriminant < 0) {
		return false;
	}
	float t0 = (-equation_B - sqrt(discriminant)) / (2 * equation_A);
	float t1 = (-equation_B + sqrt(discriminant)) / (2 * equation_A);

	bool ist0Valid = t0 > ray.t_min && t0 < ray.t_max && !(bounded && (ray.pointAtTime(t0).z > 1 || ray.pointAtTime(t0).z < -1));
	bool ist1Valid = t1 > ray.t_min && t1 < ray.t_max && !(bounded && (ray.pointAtTime(t1).z > 1 || ray.pointAtTime(t1).z < -1));

	if (ist0Valid || ist1Valid)
	{
		return true;
	}
	return false;
}