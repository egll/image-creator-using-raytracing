#include <windows.h>
#include "Derived.h"

Scene* scene;
int main(int argc, char* argv[]) {
	if (argc != 2)
	{
		scene = new Scene();
	}
	else
	{
		scene = new Scene(argv[1]);
	}
	scene->initialize();
	scene->render();
	Sleep(1000);
	return 0;
}