#include "IOMethods.h"
#include <iostream>
#include <fstream>
#include <sstream>
#include <deque>

bool IOMethods::readValues(std::stringstream &s, const int numvals, float* values)
{
	for (int i = 0; i < numvals; i++) {
		s >> values[i];
		if (s.fail()) {
			std::cout << "Failed reading value " << i << " from string " << s.str().c_str() << " will skip\n";
			return false;
		}
	}
	return true;
}
