#ifndef EAGLE_RAYTRACING_MATH_METHODS
#define EAGLE_RAYTRACING_MATH_METHODS
#include "Basic.h"

const float MATH_PI = 3.1415926535897932;

class MathMethods
{
public:
	static float dot(const Vector& vec3_1, const Vector& vec3_2);
	static float dot(const Vector4& vec_1, const Vector4& vec_2);
	static Vector cross(const Vector& vec3_1, const Vector& vec3_2);
	static Matrix inverse(const Matrix& matrix);
	static Matrix transpose(const Matrix& matrix);
	static Matrix inverseTranspose(const Matrix & matrix);
	static float determinant(const Matrix& matrix3);
	static float radians(const float& angle);
	static Vector oppositeDirection(const Vector& vector);
	static Matrix translation(float x, float y, float z);
	static Matrix rotation(float x, float y, float z, float angle);
	static Matrix scaling(float x, float y, float z);
	static float length(const Vector& v);
};

#endif