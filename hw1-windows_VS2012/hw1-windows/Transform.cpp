// Transform.cpp: implementation of the Transform class.


#include "Transform.h"
#include <stdio.h>
#include <iostream>

//Please implement the following functions:

// Helper rotation function.  
mat3 Transform::rotate(const float degrees, const vec3& axis) {
	float convert_radians = glm::radians(degrees);

	float cos_angle = glm::cos(convert_radians);
	float sin_angle = glm::sin(convert_radians);

	glm::mat3x3 identity3x3 = mat3(1.0f);
	glm::mat3x3 mat_times_transpose = glm::mat3(axis.x * axis.x, axis.x * axis.y, axis.x * axis.z, axis.x * axis.y, axis.y * axis.y, axis.y * axis.z, axis.x * axis.z, axis.y*axis.z, axis.z*axis.z);
	glm::mat3x3 conjugate_transpose = glm::mat3(0, axis.z, -axis.y, -axis.z, 0, axis.x, axis.y, -axis.x, 0);
	
	return ((cos_angle * identity3x3) + ((1- cos_angle) * mat_times_transpose) + (sin_angle * conjugate_transpose));
}

// Transforms the camera left around the "crystal ball" interface
void Transform::left(float degrees, vec3& eye, vec3& up) {
	eye = eye * rotate(degrees, -glm::normalize(up));
	printf("Coordinates: %.2f, %.2f, %.2f; distance: %.2f\n", eye.x, eye.y, eye.z, sqrt(pow(eye.x, 2) + pow(eye.y, 2) + pow(eye.z, 2)));
}

// Transforms the camera up around the "crystal ball" interface
void Transform::up(float degrees, vec3& eye, vec3& up) {
	vec3 rotation_axis = glm::normalize(glm::cross(eye, up));
	mat3 rotation = rotate(degrees, rotation_axis);
	eye = rotation * eye;
	up = rotation * up;
}

// Your implementation of the glm::lookAt matrix
mat4 Transform::lookAt(vec3 eye, vec3 up) {
	vec3 w = glm::normalize(eye);
	vec3 u = glm::cross(up, w) / glm::length(glm::cross(up, w));
	vec3 v = glm::cross(w, u);

	mat4 matrix_x4 = glm::mat4x4(u.x, v.x, w.x, 0, u.y, v.y, w.y, 0, u.z, v.z, w.z, 0, 0, 0, 0, 1);
	mat4 transformation_mat4 = glm::mat4x4(1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, -eye.x, -eye.y, -eye.z, 1);
	return matrix_x4 * transformation_mat4;
}

Transform::Transform()
{

}

Transform::~Transform()
{

}
